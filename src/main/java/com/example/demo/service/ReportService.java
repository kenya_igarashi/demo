package com.example.demo.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		return reportRepository.findAll(Sort.by(Sort.Direction.DESC, "updated"));
	}

	// レコード追加、編集
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコード単件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// レコード範囲取得
	public List<Report> findBetweenReport(String start, String end) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Report since = new Report();
		Report until = new Report();
		Date startDate = null;
		Date endDate = null;
		// 投稿取得ここに絞り込みデータ投下
		if (start.isBlank()) {
			start += "2020/01/01";
		}else {
			start += " 00:00:00";
			start = start.replace("-", "/");
		}
		if (end.isBlank()) {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			end = format.format(timestamp);
		}else {
			end += " 00:00:00";
			end = end.replace("-", "/");
		}
		try {
			startDate = format.parse(start);
			endDate = format.parse(end);
			since.setUpdated_date(startDate);
			until.setUpdated_date(endDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return reportRepository.findByUpdatedBetween(startDate, endDate);
	}

}
