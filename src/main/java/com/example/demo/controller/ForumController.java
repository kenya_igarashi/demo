package com.example.demo.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	Date now = new Date(); //現在日時
	SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	String formatNow = format.format(now);

	// 投稿、コメント内容表示画面
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport();
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// idを更新するentityにセット
		report.setUpdated_date(now);
		report.setCreated_date(now);
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		// 削除をテーブルに格納
		reportService.deleteReport(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		// idを更新するentityにセット
		report.setId(id);
		// 編集した投稿を更新
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 投稿編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Report report = reportService.editReport(id);
		// 編集する投稿をセット
		mav.addObject("formModel", report);
		// 画面遷移先を指定
		mav.setViewName("/edit");
		return mav;
	}

	// 新規コメント画面
	@GetMapping("/comment/{report_id}")
	public ModelAndView newComment(@PathVariable Integer report_id) {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Comment comment = new Comment();
		mav.addObject("formReportId", report_id);
		// 準備したentityを保管
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/comment");
		return mav;
	}

	// コメント処理
	@PostMapping("/addComment")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment,
			@RequestParam("report_id") Integer report_id) {
		// idを更新するentityにセット
		comment.setReport_id(report_id);
		// コメントをテーブルに格納
		commentService.saveComment(comment);
		// idを更新するentityにセット
		// report_idで単体呼び出し、セットで上書きして再送
		Report report = reportService.editReport(report_id);
		report.setId(report_id);
		report.setUpdated_date(now);
		// 更新日時をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント編集画面
	@GetMapping("/commentEdit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Comment comment = commentService.editComment(id);
		// 編集する投稿をセット
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/commentEdit");
		return mav;
	}

	// コメント編集処理
	@PutMapping("/commentUpdate/{id}")
	public ModelAndView updateComment(@ModelAttribute("formModel") Comment comment,
			@RequestParam("report_id") Integer report_id) {
		// idを更新するentityにセット
		comment.setReport_id(report_id);
		// 編集した投稿を更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント削除処理
	@DeleteMapping("/commentDelete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		// 削除をテーブルに格納
		commentService.deleteComment(id);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// 期間による表示絞り込み
	@GetMapping("/search")
	public ModelAndView betweenTop(@RequestParam("start") String start, @RequestParam("end") String end) {
		ModelAndView mav = new ModelAndView();
		//ここでIllegalArgumentException: Parameter value [com.example.demo.entity.Report@68d8d20c]
		//did not match expected type [java.util.Date (n/a)]
		List<Report> contentData = reportService.findBetweenReport(start, end);
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

}
